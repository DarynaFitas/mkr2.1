﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mrk
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string input = numbersTextBox.Text;
            string[] numbers = input.Split(' ');

            double[] array = new double[numbers.Length];

            for (int i = 0; i < numbers.Length; i++)
            {
                if (double.TryParse(numbers[i], out double number))
                {
                    array[i] = number;
                }
                else
                {
                    MessageBox.Show("Введено некоректні дані.");
                    return;
                }
            }

            double sum = 0;
            bool negativeFound = false;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                {
                    negativeFound = true;
                    break;
                }

                sum += array[i];
            }

            if (negativeFound)
            {
                resultLabel.Text = "Сума елементів до першого від'ємного числа: " + sum.ToString();
            }
            else
            {
                resultLabel.Text = "У масиві немає від'ємних чисел.";
            }
        }
    }
}
